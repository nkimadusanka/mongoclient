/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopclient;

import com.mongodb.client.MongoDatabase;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import controller.CollectionProvider;
import controller.model.CustomerController;
import controller.DummyGenerator;
import controller.FrameController;
import controller.MongoConController;
import controller.model.BookCateController;
import controller.model.BookController;
import controller.model.InvoiceController;
import excep.DummyException;
import java.net.URI;
import javax.ws.rs.core.MediaType;
import neo4j.NeoEngin;

/**
 *
 * @author isumlk
 */
public class ShopClient {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      
       try{
          //csv 700 10 5 20 2010
          
          String SERVER_ROOT_URI = "http://localhost:7474/db/data/";
          
          MongoConController connection = new MongoConController("localhost", 27017,"bookshop");
            
          MongoDatabase db = MongoConController.getConnection();
          CollectionProvider.initCollectionProvider(db);
          
          NeoEngin neoEngin = new NeoEngin(SERVER_ROOT_URI);
            
          DummyGenerator dummyGenerator = new DummyGenerator(neoEngin,args[0],Integer.parseInt(args[1]),Integer.parseInt(args[2]),Integer.parseInt(args[3]),Integer.parseInt(args[4]),Integer.parseInt(args[5]));
          dummyGenerator.generateDummyData(db);
          
          CustomerController custCont = new CustomerController(CollectionProvider.getCustdb());
          BookCateController bokCatCont = new BookCateController(CollectionProvider.getBook_catdb());
          BookController bokCont = new BookController(CollectionProvider.getBookdb(),CollectionProvider.getInvdb());
          InvoiceController invCont = new InvoiceController(CollectionProvider.getInvdb());
          
          FrameController fc = new FrameController(neoEngin,custCont,bokCatCont,bokCont,invCont);
          
       }catch(DummyException e){
           System.out.println(e.toString());
       } catch(RuntimeException e){
           System.out.println("Please unlock the neo4j lock");
           e.printStackTrace();
       }
   }
   
}
