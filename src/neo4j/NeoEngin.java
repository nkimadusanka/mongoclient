/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo4j;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import frames.HighestFrame;
import java.util.Date;
import javax.ws.rs.core.MediaType;
import org.bson.Document;
import org.joda.time.DateTime;
import org.neo4j.shell.util.json.JSONArray;
import org.neo4j.shell.util.json.JSONException;
import org.neo4j.shell.util.json.JSONObject;

/**   
 *
 * @author isuru
 */
public class NeoEngin {
    
   private int root;
    private String SERVER_ROOT_URI;
    private WebResource resource;
    
    public NeoEngin(String SERVER_ROOT_URI) {
        this.SERVER_ROOT_URI = SERVER_ROOT_URI + "transaction/commit";
        this.resource = Client.create().resource(this.SERVER_ROOT_URI);
        initDB();
    }
    
    private void initDB(){
        String cql =    "MATCH (n) " +
                        "OPTIONAL MATCH (n)-[r]-()" +
                        "DELETE n,r";
        ClientResponse response = resource.accept( MediaType.APPLICATION_JSON ).type( MediaType.APPLICATION_JSON ).entity(this.getPayload(cql)).post(ClientResponse.class );
        if(response.getStatus() == 200){
           System.out.println("All the nodes and relations are deleted");
        }
        response.close();
        createRootNode();
    }
    
    private void createRootNode(){
       String cql = "CREATE(s:SHOP) RETURN ID(s)";
       ClientResponse response = resource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).entity(this.getPayload(cql)).post(ClientResponse.class);
       try{
          String row;
          JSONObject tmp = new JSONObject(response.getEntity(String.class));
          JSONArray tmpArray;
          JSONArray result = tmp.getJSONArray("results");
          for(int i = 0;i < result.length();++i){
             tmpArray = ((JSONObject) result.get(i)).getJSONArray("data");
             tmp = tmpArray.getJSONObject(i);
             row = tmp.get("row").toString();
             root = Integer.parseInt(row.substring(1, row.length()-1));
          }
       }catch(JSONException e){
          System.out.println("Error in creating Root object");
          e.printStackTrace();
       }
       response.close();
    }
    
    private String getPayload(String cql){
       return "{\"statements\" : [ {\"statement\" : \"" + cql + "\"} ]}";
    }
    
    private ClientResponse exectueCql(String cql){
       ClientResponse response = resource.accept( MediaType.APPLICATION_JSON ).type( MediaType.APPLICATION_JSON ).entity(this.getPayload(cql)).post(ClientResponse.class);
       return response;
    }
    
    public void addCustomer(Document document){
        String cql = "CREATE (cust:"+"CUSTOMER";
        cql += " { " +
                "no:\'"+document.get("_id")+"\' ,"+
                " name: \'"+document.get("name")+"\'"+
                " })";
        ClientResponse response = this.exectueCql(cql);
        if(response.getStatus() != 200){
           System.out.println("Error in creating customer("+response.getStatus()+"):"+document.toJson().toString());
        }
        response.close();
    }
    
    
    public void addBookCat(Document document) {
        
        String cql = "CREATE (bok_cat:"+"BOOK_CAT";
        cql += " { " +
                "no:'"+document.get("_id")+"' ,"+
                "category_name:'"+document.get("category_name")+"'"+
                " })";
        ClientResponse response = this.exectueCql(cql);
        if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+"):"+document.toJson().toString());
        }
        response.close();
    }
    
    public void createInvoice(Document invDoc){
       DateTime dt = new DateTime(invDoc.get("date")) ;
       
       this.createYear(dt.getYear()+"");
       this.createMonth(dt.getYear()+"", dt.getMonthOfYear()+"");
       this.createInvoiceGraph(dt.getYear()+"",dt.getMonthOfYear()+"",invDoc);
    }
    
    private void createYear(String year){
       
       ClientResponse response;
       
       String cql = "MERGE (y:YEAR{year:"+ year +"})";
       response = exectueCql(cql);
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
       
       cql = "MATCH (y:YEAR),(s:SHOP) WHERE ID(s) = "+ this.root +" AND y.year = "+ year +" MERGE s-[:HAVE]->y RETURN y,s";
       response = exectueCql(cql);
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
    }
    
    public void createMonth(String year,String month){
       
       String cql;
       ClientResponse response;
       
       cql = "MERGE (m:MONTH{month:"+month+",year:"+year+"})";
       response = exectueCql(cql);
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
       
       cql = "MATCH (y:YEAR),(m:MONTH) WHERE y.year = "+ year +" AND m.year = "+ year +" AND m.month = "+ month +" MERGE y-[r:HAS]->m RETURN y,m,r";
       response = exectueCql(cql);
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
    }
    
    public void createInvoiceGraph(String year,String month,Document invDoc) {
       float total = 0;
       String cql;
       ClientResponse response;
       
       Document cust = (Document) invDoc.get("customer");
       
       cql = "MERGE (c:CUSTOMER{name:\'"+cust.getString("name")+"\'})"; 
       
       response = exectueCql(cql);
       
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
       
       cql = "MATCH (c:CUSTOMER),(m:MONTH), ()-[r:BUY]->() WHERE c.name = \'"+ cust.getString("name") +"\' AND m.month = "+month+" AND m.year = "+year+" RETURN DISTINCT r.total";
       
       response = exectueCql(cql);
       
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       try{
          String row;
          JSONObject tmp = new JSONObject(response.getEntity(String.class));
          JSONArray tmpArray;
          JSONArray result = tmp.getJSONArray("results");
          for(int i = 0;i < result.length();++i){
             tmpArray = ((JSONObject) result.get(i)).getJSONArray("data");
             tmp = tmpArray.getJSONObject(i);
             row = tmp.get("row").toString();
             total = Float.parseFloat(row.substring(1, row.length()-1));
          }
       }catch(JSONException e){
          total = 0;
       }
       
       response.close();
       
       cql = "MATCH (m:MONTH),(c:CUSTOMER),(m)-[r:BUY]->(c) WHERE m.month = "+month+" AND m.year = "+year+" AND c.name = \'"+ cust.getString("name") +"\' DELETE r";
       
       response = exectueCql(cql);
       
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
       
       cql = "MATCH (m:MONTH),(c:CUSTOMER) WHERE m.month = "+month+" AND m.year = "+year+" AND c.name = \'"+ cust.getString("name") +"\' MERGE m-[r:BUY{total:"+(total + Float.parseFloat(invDoc.get("gross_total").toString()))+"}]->c RETURN m,c";
       
       response = exectueCql(cql);
       
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       response.close();
   }
    
    public void getHighestPaid(HighestFrame highestFrame){
       ClientResponse response;
       String[] tmpRsltArray = null;
       String tmpRest;
       
       DateTime now = new DateTime(new Date());
       now = now.minusMonths(1);
       
       String cql = "MATCH (c:CUSTOMER),(m:MONTH),(m)-[r:BUY]->(c) WHERE m.year = "+ now.getYear() +" AND m.month = "+now.getMonthOfYear()+" RETURN DISTINCT MAX(r.total),c.name LIMIT 1";
       response = exectueCql(cql);
       
       if(response.getStatus() != 200){
           System.out.println("Error in creating Book Category("+response.getStatus()+")");
       }
       
       try{
          String row;
          JSONObject tmp = new JSONObject(response.getEntity(String.class));
          JSONArray tmpArray;
          JSONArray result = tmp.getJSONArray("results");
          for(int i = 0;i < result.length();++i){
             tmpArray = ((JSONObject) result.get(i)).getJSONArray("data");
             tmp = tmpArray.getJSONObject(i);
             tmpRest = tmp.get("row").toString();
             tmpRest = tmpRest.substring(1, tmpRest.length()-1);
             tmpRsltArray = tmpRest.split(",");
             
             highestFrame.setTotal(tmpRsltArray[0]);
             highestFrame.setCustomer(tmpRsltArray[1]);
             highestFrame.setDateTime(now);
          }
       }catch(JSONException e){
          
       }
       
       response.close();
    }
    
}
