/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author isumlk
 */
public class DummyDateProvider {
   
   public static Calendar getRandomDate(int yEnd){
      
      int year,month,day;
      int maxMonth = 12,maxDay;
      
      GregorianCalendar gc = new GregorianCalendar();
      Calendar now = Calendar.getInstance();
      
      year = randBetween(yEnd, now.get(Calendar.YEAR));
      
      if(year == now.get(Calendar.YEAR)){
         maxMonth = now.get(Calendar.MONTH);
      }
      
      month = randBetween(1, maxMonth);
      
      Calendar selDate = new GregorianCalendar(year, month, 1);
      
      maxDay = selDate.getActualMaximum(Calendar.DAY_OF_MONTH);
      
      if((year == now.get(Calendar.YEAR)) && (month == now.get(Calendar.MONTH))){
         maxDay = now.get(Calendar.DATE);
      }
      day = randBetween(1, maxDay);
      return new GregorianCalendar(year, (month - 1), day);
   }
   
   public static int randBetween(int start,int end){
      return start + (int)Math.round(Math.random() * (end - start));
   }
}
