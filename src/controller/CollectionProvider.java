/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 *
 * @author isumlk
 */
public class CollectionProvider {
   
   private static MongoDatabase db;
   private static MongoCollection<Document> bookdb;
   private static MongoCollection<Document> book_catdb;
   private static MongoCollection<Document> custdb;
   private static MongoCollection<Document> invdb;

   public static void initCollectionProvider(MongoDatabase db) {
      CollectionProvider.db = db;
      
      CollectionProvider.book_catdb = db.getCollection("book_category");
      CollectionProvider.bookdb = db.getCollection("book");
      CollectionProvider.custdb = db.getCollection("customer");
      CollectionProvider.invdb = db.getCollection("invoice");
   }

   public static MongoCollection<Document> getBookdb() {
      return bookdb;
   }

   public static void setBookdb(MongoCollection<Document> bookdb) {
      CollectionProvider.bookdb = bookdb;
   }

   public static MongoCollection<Document> getBook_catdb() {
      return book_catdb;
   }

   public static void setBook_catdb(MongoCollection<Document> book_catdb) {
      CollectionProvider.book_catdb = book_catdb;
   }

   public static MongoCollection<Document> getCustdb() {
      return custdb;
   }

   public static void setCustdb(MongoCollection<Document> custdb) {
      CollectionProvider.custdb = custdb;
   }

    public static MongoCollection<Document> getInvdb() {
        return invdb;
    }

    public static void setInvdb(MongoCollection<Document> invdb) {
        CollectionProvider.invdb = invdb;
    }
    
}
