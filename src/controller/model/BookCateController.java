/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.model;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import org.bson.Document;

/**
 *
 * @author isumlk
 */
public class BookCateController {
   
   private MongoCollection<Document> bokCatDb;

   public BookCateController(MongoCollection<Document> bokCatDb) {
      this.bokCatDb = bokCatDb;
   }
   
   public FindIterable<Document> getAllCategories(){
      return bokCatDb.find();
   }
}
