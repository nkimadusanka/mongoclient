/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.model;

import com.mongodb.BasicDBList;
import com.mongodb.client.MongoCollection;
import controller.DummyDateProvider;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import model.SelectItem;
import org.bson.Document;

/**
 *
 * @author isumlk
 */
public class InvoiceController {
   
   private MongoCollection<Document> invDb;

   public InvoiceController(MongoCollection<Document> invDb) {
      this.invDb = invDb;
   }
   
   public void addInvoice(Document cust,ArrayList<SelectItem> selectItemList){
      
      float total = 0;
      float tmpAmount;
      
      Document invDoc;
      
      Document purItDoc;
      Document tmpBook;
      
      BasicDBList pItList = new BasicDBList();
      
      for(int i = 0;i < selectItemList.size();++i){
         
         tmpBook = selectItemList.get(i).getBook();
         tmpAmount = selectItemList.get(i).getAmount();
         
         purItDoc = new Document();
         
         purItDoc.put("book", tmpBook);
         purItDoc.put("quantity", tmpAmount);
         purItDoc.put("amount", tmpAmount * Float.parseFloat(tmpBook.get("price").toString()));
         
         pItList.add(purItDoc);
         
         total += tmpAmount * Float.parseFloat(tmpBook.get("price").toString());
      }
      
      invDoc = new Document();
      invDoc.append("customer", cust);
      invDoc.append("date", DummyDateProvider.getRandomDate(Calendar.getInstance().get(Calendar.YEAR)).getTime());
      invDoc.append("discount", 5);
      invDoc.append("gross_total", total);
      invDoc.append("purch_list", pItList);
      
      invDb.insertOne(invDoc);
   }
}
