/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.model;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.util.JSON;
import controller.CollectionProvider;
import java.awt.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.joda.time.DateTime;
import org.neo4j.shell.util.json.JSONArray;
import org.neo4j.shell.util.json.JSONException;
import org.neo4j.shell.util.json.JSONObject;

/**
 *
 * @author isumlk
 */
public class BookController {
   
   private final MongoCollection<Document> bokDb;
   private final MongoCollection<Document> invDb;

   public BookController(MongoCollection<Document> bokDb,MongoCollection<Document> invDb) {
      this.bokDb = bokDb;
      this.invDb = invDb;
   }
   
   public FindIterable<Document> getExpectBookList(BasicDBObject notIn){
      
      return bokDb.find(notIn).limit(2);
   }
   
   public FindIterable<Document> getBooksByCategory(Document bokCat){
       
        BasicDBObject bKey = new BasicDBObject();
        bKey.append("b_cat", bokCat);
        return bokDb.find(bKey);
   }
   
   public HashMap<String,ArrayList<String>> getRecomendedBooks(Document customer){
      
      FindIterable<Document> invList;
      HashMap<String,ArrayList<String>> catMap = new HashMap<>();
      BasicDBObject bObject,tmp;
      JSONObject tmpJs;
      JSONArray tmpArrayJs;
      
      String b_id,cat_name;
      
      DateTime now = new DateTime(new Date());
      now = now.minusMonths(2);
      
      bObject = new BasicDBObject();
      bObject.append("customer._id", customer.get("_id"));
      tmp = new BasicDBObject();
      tmp.append("$gt", now.toDate());
      bObject.append("date", tmp);
      
      invList = invDb.find(bObject);
      
      try{
         for (Document inv : invList) {
            tmpJs = new JSONObject(JSON.serialize(inv));
            tmpArrayJs = tmpJs.getJSONArray("purch_list");
            for (int i = 0; i < tmpArrayJs.length(); i++) {
               JSONObject purch_Item = (JSONObject) tmpArrayJs.get(i);
               JSONObject book = (JSONObject) purch_Item.get("book");
               b_id = (String) book.get("_id").toString();
               JSONObject bok_cat = (JSONObject) book.get("b_cat");
               cat_name = (String) bok_cat.get("category_name");
               if(!catMap.containsKey(cat_name)){
                  catMap.put(cat_name, new ArrayList<String>());
               }
               catMap.get(cat_name).add(b_id);
            }
         }
         
      }catch(JSONException e){
         System.out.println("Invalid json formate found");
         e.printStackTrace();
      }
      return catMap;
   }
   
}
