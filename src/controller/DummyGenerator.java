/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import excep.DummyException;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;
import model.Book;
import model.BookCate;
import model.Customer;
import model.Invoice;
import neo4j.NeoEngin;
import org.bson.Document;

/**
 *
 * @author isumlk
 */
public class DummyGenerator {
    
    private final String csvPath;
    private MongoDatabase db;
    private int maxInvoice;
    private int maxNoBookEntry;
    private int maxQuantity;
    private int yEnd;
    private int maxDiscount;
    
    private static final ArrayList<Customer> cstList = new ArrayList<>();
    private static final ArrayList<BookCate> bCatList = new ArrayList<>();
    private static final ArrayList<Book> bList = new ArrayList<>();
    private static final ArrayList<Invoice> iList = new ArrayList<>();
    
    private NeoEngin engin;
    
    public DummyGenerator(NeoEngin neoEngin,String csvPath,int maxInvoice,int maxNoBookEntry,int maxQuantity,int maxDiscount,int yEnd){
        this.csvPath = csvPath;
        this.maxInvoice = maxInvoice;
        this.maxNoBookEntry = maxNoBookEntry;
        this.maxQuantity = maxQuantity;
        this.yEnd = yEnd;
        this.maxDiscount = maxDiscount;
        
        this.engin = neoEngin;
    }
    
    public void generateDummyData(MongoDatabase db) throws DummyException{
        this.db = db;
        dropCollections();
        loadingCSV();
        loadInvoices();
    }
    
    private void dropCollections(){
        MongoCollection tmpCol;
        MongoIterable<String> colList = db.listCollectionNames();
        for (String col : colList) {
            tmpCol  = db.getCollection(col);
            tmpCol.drop();
        }
    }
    
    private void loadingCSV() throws DummyException{
        ArrayList<File> csvList = this.getFileList();
        
        if(csvList.size() != 3){
            throw new DummyException("Invalid number of csv files");
        }else{
            for (File file : csvList) {
                switch(file.getName()){
                    case "customer.csv":
                        loadCustFrCSV(file);
                        break;
                    case "book_cate.csv":
                        loadBookCatFrCSV(file);
                        break;
                }
            }
            for (File file : csvList) {
                switch(file.getName()){
                    case "book.csv":
                        loadBookFrCSV(file);
                        break;
                }
            }
            saveCustToMongo();
            saveBoCatToMongo();
            saveBookToMong();
            createInvoiceMongo();
        }
    }
    
    private ArrayList<File> getFileList(){
        
        File dir = new File(csvPath);
        File[] fList = dir.listFiles();
        
        ArrayList<File> cfList = new ArrayList<>();
        
        for (File file : fList) {
            if(file.isFile()||file.getName().contains(".csv")){
                cfList.add(file);
            }
        }
        return cfList;
    }

    private void loadCustFrCSV(File file) throws DummyException {
        
        Customer tmpCust;
        String tmp;
        
        try{
            Scanner s = new Scanner(file);
            while(s.hasNextLine()){
                tmp = s.nextLine();
                if(tmp.length() > 0){
                    tmpCust = new Customer(s.nextLine());
                    cstList.add(tmpCust);
                }                
            }
        }catch(FileNotFoundException e){
            throw new DummyException("CSV File not Found:"+file.getName());
        }
    }
    
    private void loadBookCatFrCSV(File file) throws DummyException{
        
        BookCate tmpBokCat;
        String tmp;
        
        try{
            Scanner s = new Scanner(file);
            while (s.hasNextLine()) {
                tmp = s.nextLine();
                if(tmp.length() > 0){
                    tmpBokCat = new BookCate(tmp);
                    bCatList.add(tmpBokCat);
                }
            }
            
        }catch(FileNotFoundException e){
            throw new DummyException("CSV File not Found:"+file.getName());
        }
        
    }
    
    private void loadBookFrCSV(File file) throws DummyException {
        
        Book book;
        BookCate bookCate;
        String tmp;
        String split = ",";
        
        try{
            Scanner s = new Scanner(file);
            while (s.hasNextLine()) {
                tmp = s.nextLine();
                if(tmp.length() > 0){
                    String[] row = tmp.split(",");
                    bookCate = bCatList.get(Integer.parseInt(row[0])-1);
                    book = new Book(row[1], bookCate, Double.parseDouble(row[2]));
                    bList.add(book);
                }
            }
            
        }catch(FileNotFoundException e){
            throw new DummyException("CSV File not Found:"+file.getName());
        }
        
        
    }
    
    private void saveCustToMongo() {
        
        Document document;
        db.createCollection("customer");
        MongoCollection<Document> custDb = db.getCollection("customer");
        
        for (Customer customer : cstList) {
            document = new Document();
            document.put("name", customer.getName());
            custDb.insertOne(document);
            //engin.addCustomer(custDb.find(document).first());
        }
        
        System.out.println("\nDummy Custormer added.............");
    }

    private void saveBoCatToMongo() {
        
        Document document;
        db.createCollection("book_category");
        MongoCollection<Document> boCatDb = db.getCollection("book_category");
        
        for (BookCate bookCat : bCatList) {
            document = new Document();
            document.put("category_name", bookCat.getB_categry());
            boCatDb.insertOne(document);
            //engin.addBookCat(boCatDb.find(document).first());
        }
        
        System.out.println("Dummy BookCategorys added.............");
        
    }

    private void saveBookToMong() {
        
        Document bDocument;
        BookCate tmpbokCat;
        db.createCollection("book");
        MongoCollection<Document> bookDb = db.getCollection("book");
        MongoCollection<Document> bookCatDb = db.getCollection("book_category");
        
        for (Book book : bList) {
           
           tmpbokCat = book.getBookCate();
           
           BasicDBObject bKey = new BasicDBObject();
           bKey.append("category_name", tmpbokCat.getB_categry());
           FindIterable<Document> boCatDoc = bookCatDb.find(bKey);
           
           for (Document boCate : boCatDoc) {
              bDocument = new Document();
              bDocument.append("book_name", book.getBookName());
              bDocument.append("price", book.getPrice());
              bDocument.append("b_cat", boCate);
              
              bookDb.insertOne(bDocument);
              
              //engin.addBook(bookDb.find(bDocument).first());
              
              break;
           }
       }
        System.out.println("Dummy Books added.............");
    }
    
    private void createInvoiceMongo() {
        db.createCollection("invoice");
    }
    
    public ArrayList<Customer> getCustomersList(){
       return cstList;
    }
    
    public ArrayList<BookCate> getBookCatList(){
       return bCatList;
    }
    
    public ArrayList<Book> getBookList(){
       return bList;
    }

   private void loadInvoices() {
      
      int entryNo;
      int bookEntry;
      int discount;
      double amount;
      Calendar cal;
      
      BasicDBObject bKey;
      
      Book book;
      int quantity;
      double total;      
      
      BasicDBList pItList;
      
      Document invDoc;
      Document customerDoc;
      Document bookDoc;
      Document purItDoc;
      
      SimpleDateFormat yyyymmdd = new SimpleDateFormat("yyyy-MM-dd");
      
      for (Customer customer : cstList) {
          
         bKey = new BasicDBObject();
         bKey.append("name", customer.getName());
         customerDoc = CollectionProvider.getCustdb().find(bKey).first();
         
         entryNo = DummyDateProvider.randBetween(1, maxInvoice);
         amount = 0;

         for(int i = 0;i < entryNo;++i){
            total = 0;

            bookEntry = DummyDateProvider.randBetween(1, maxNoBookEntry);
            cal = DummyDateProvider.getRandomDate(yEnd);
            discount = (int) Math.round(Math.random() * maxDiscount);
            
            pItList = new BasicDBList();

            for (int j = 0; j < bookEntry; j++) {
               book = bList.get((int) Math.round(Math.random() * (bList.size() - 1)));
               quantity = (int) Math.round(Math.random() * maxQuantity);
               amount = book.getPrice() * quantity;
               total += amount;
               
               bKey = new BasicDBObject();
               bKey.append("book_name", book.getBookName());
               bookDoc = CollectionProvider.getBookdb().find(bKey).first();
               
               purItDoc = new Document();
               
               purItDoc.put("book", bookDoc);
               purItDoc.put("quantity", quantity);
               purItDoc.put("amount", amount);
               
               pItList.add(purItDoc);
            }
            
            invDoc = new Document();
            invDoc.append("customer", customerDoc);
            invDoc.append("date", cal.getTime());
            invDoc.append("discount", discount);
            invDoc.append("gross_total", total);
            invDoc.append("purch_list", pItList);
            
            CollectionProvider.getInvdb().insertOne(invDoc);
            engin.createInvoice(invDoc);
         }
      }
      System.out.println("Dummy Invoices added.............");
   }
}
