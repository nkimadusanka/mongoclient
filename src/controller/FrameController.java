/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.model.BookCateController;
import controller.model.BookController;
import controller.model.CustomerController;
import controller.model.InvoiceController;
import frames.HomeFrame;
import frames.SelectBookFrame;
import frames.SelectCategoryFrame;
import frames.SelectCustomerFrame;
import java.awt.Dialog;
import java.awt.Frame;
import javax.swing.JFrame;
import neo4j.NeoEngin;
import org.bson.Document;


/**
 *
 * @author isumlk
 */
public class FrameController {
   
   private HomeFrame home;
   private SelectBookFrame selectBookFrame;
   private SelectCategoryFrame categoryFrame;
   private SelectCustomerFrame customerFrame;
   
   private CustomerController custCont;
   private BookCateController bokCatCont;
   private BookController bokCont;
   private InvoiceController invCont;
   
   private Document customer;
   
   private NeoEngin neoEngin;

   public FrameController(NeoEngin neoEngin,CustomerController custCont,BookCateController bokCatCont,BookController bokCont,InvoiceController invCont) {
      
      this.custCont = custCont;
      this.bokCatCont = bokCatCont;
      this.bokCont = bokCont;
      this.invCont = invCont;
      
      this.neoEngin = neoEngin;
      
      this.initHome("Home of the online shopping center");
      
      this.customerFrame = new SelectCustomerFrame((Frame)home, true,custCont);
      this.customerFrame.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
      this.customerFrame.setVisible(true);
      
      this.customer = customerFrame.getCustomer();
      
      this.home.setCustomer(this.customer);
      
      this.refreshHome();
   }
   
   private void initHome(String title){
      home = new HomeFrame(title,this.customer,custCont,bokCatCont,bokCont,invCont,this.neoEngin);
      home.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      home.setVisible(true);
   }
   
   private void refreshHome(){
      
      
      
   }
}
