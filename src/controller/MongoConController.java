/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 *
 * @author isumlk
 */
public class MongoConController {
   
   private static String hostname;
   private static int port;
   private static String database;
   private static MongoDatabase db;
   
   public MongoConController(String hostname, int port,String database) {
      this.hostname = hostname;
      this.port = port;
      this.database = database;
   }
   
   public static MongoDatabase getConnection(){
      if(db == null){
         MongoClient client = new MongoClient(hostname, port);
         client.dropDatabase(database);
         System.out.println("Drop the "+database+"..................");
         db = client.getDatabase(database);
         System.out.println("Create the "+database+".................."); 
      }
      return db;
   }
   
}
