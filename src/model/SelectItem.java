/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.bson.Document;

/**
 *
 * @author isumlk
 */
public class SelectItem {
   
   private Document book;
   private int amount;

   public SelectItem(Document book, int amount) {
      this.book = book;
      this.amount = amount;
   }

   public Document getBook() {
      return book;
   }

   public void setBook(Document book) {
      this.book = book;
   }

   public int getAmount() {
      return amount;
   }

   public void setAmount(int amount) {
      this.amount = amount;
   }
   
}
