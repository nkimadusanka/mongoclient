/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author isuru
 */
public class Book{
    
    private String bookName;
    private BookCate bookCate;
    private double price;

    public Book(String bookName, BookCate bookCate, double price) {
        this.bookName = bookName;
        this.bookCate = bookCate;
        this.price = price;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public BookCate getBookCate() {
        return bookCate;
    }

    public void setBookCate(BookCate bookCate) {
        this.bookCate = bookCate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
}
