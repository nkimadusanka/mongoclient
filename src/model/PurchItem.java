/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author isumlk
 */
public class PurchItem {
   
   private Book book;
   private double amount;
   private int quantity;

   public PurchItem(Book book,int quantity, double amount) {
      this.book = book;
      this.quantity = quantity;
      this.amount = amount;
   }

   public Book getBook() {
      return book;
   }

   public void setBook(Book book) {
      this.book = book;
   }

   public double getAmount() {
      return amount;
   }

   public void setAmount(double amount) {
      this.amount = amount;
   }

   public int getQuantity() {
      return quantity;
   }

   public void setQuantity(int quantity) {
      this.quantity = quantity;
   }
   
}
