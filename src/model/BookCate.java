/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author isuru
 */
public class BookCate {
    
    private String b_categry;

    public BookCate(String b_categry) {
        this.b_categry = b_categry;
    }

    public String getB_categry() {
        return b_categry;
    }

    public void setB_categry(String b_categry) {
        this.b_categry = b_categry;
    }
    
}
