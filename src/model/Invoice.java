/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author isumlk
 */
public class Invoice {
   
   private Customer cust;
   private Calendar iDate;
   private double discount;
   private double gross_total;
   private double net_Total;
   private ArrayList<PurchItem> purchList;

   public Invoice(Customer cust, Calendar iDate, double discount,double gross_Total, double net_Total, ArrayList<PurchItem> purchList) {
      this.cust = cust;
      this.iDate = iDate;
      this.discount = discount;
      this.gross_total = gross_Total;
      this.net_Total = net_Total;
      this.purchList = purchList;
   }

   public Customer getCust() {
      return cust;
   }

   public void setCust(Customer cust) {
      this.cust = cust;
   }

   public Calendar getiDate() {
      return iDate;
   }

   public void setiDate(Calendar iDate) {
      this.iDate = iDate;
   }

   public double getDiscount() {
      return discount;
   }

   public void setDiscount(double discount) {
      this.discount = discount;
   }

   public double getNet_Total() {
      return net_Total;
   }

   public void setNet_Total(double net_Total) {
      this.net_Total = net_Total;
   }

   public ArrayList<PurchItem> getPurchList() {
      return purchList;
   }

   public void setPurchList(ArrayList<PurchItem> purchList) {
      this.purchList = purchList;
   }

   public double getGross_total() {
      return gross_total;
   }

   public void setGross_total(double gross_total) {
      this.gross_total = gross_total;
   }
   
}
